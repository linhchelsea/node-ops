var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', (req, res) => {
  return res.status(200).send('Welcome to my server');
});

module.exports = router;
